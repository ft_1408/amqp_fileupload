<?php
namespace Components;

include "../vendor/autoload.php";

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

// Наследуем наш класс от стандартного UploadHandler
class RabbitUpload extends UploadHandler {

    private $filePath;
    private $meta = array();
    private $amqp_srv = array();
    private $amqp_config = array();

    function __construct($options = null, $initialize = true, $error_messages = null) {

        // Настройки по умолчанию
        if(!array_key_exists('amqp', $options)){

            $this->amqp_srv['host'] = '192.168.2.110';
            $this->amqp_srv['port'] = 5672;
            $this->amqp_srv['user'] = 'guest';
            $this->amqp_srv['password'] = 'guest';
            $this->amqp_srv['vhost'] = '/';

            $this->amqp_config['queue'] = 'file_queue';
            $this->amqp_config['exchange'] = 'files_sender';
            $this->amqp_config['exchange_type'] = 'topic';
            $this->amqp_config['routing_key'] = 'sended';
        }else{
            // Получаем настройки подключения
            $this->amqp_srv = $options['amqp']['srv'];

            // Очереди
            $this->amqp_config = $options['amqp']['config'];
        }

        // Если переданны какие - то метаданные
        // сохраняем их в соотвестствующей переменной
        if(array_key_exists('meta', $options)){
            $this->meta = $options['meta'];
        }

        // Подключение к message broker
        $this->connection = new AMQPConnection($this->amqp_srv['host'], $this->amqp_srv['port'], $this->amqp_srv['user'], $this->amqp_srv['password'], $this->amqp_srv['vhost']);
        // Создадим канал
        $this->channel = $this->connection->channel();
        // Создадим exchange
        $this->channel->exchange_declare($this->amqp_config['exchange'], $this->amqp_config['exchange_type'], false, false, false);
        // Создадим очередь
        $this->channel->queue_declare($this->amqp_config['queue'], false, false, false, false);
        // Создадим бинд обменника и очереди по ключу
        $this->channel->queue_bind($this->amqp_config['queue'], $this->amqp_config['exchange'], $this->amqp_config['routing_key']);

        // Вызываем родительский метод - конструктор
        parent::__construct($options, $initialize, $error_messages);
    }

    function handle_file_upload($uploaded_file, $name, $size, $type, $error,
            $index = null, $content_range = null){

        // Вызываем родительский метод
        $file = parent::handle_file_upload($uploaded_file, $name, $size, $type, $error,
            $index, $content_range);

        // Получаем путь к загруженному файлу
        $file_path = parent::get_upload_path($file->name);
        $this->sendFile($file_path, $file->name);


        return $file;
    }

    // Метод подготавливает данные к отправке
    protected function sendFile($file_path, $name){
        // Пытаемся получить контент файла
        $content = $this->getFileContent($file_path);

        // Если получилось получить содержимое файла
        if($content){

            // Формируем массив с данными для отправки
            $data = array('file_name' => $name,
                          'content' => $content,
                          'meta' => $this->meta);

            // Вызываем метод для отправки сериализованного массива
            $this->publishFile(serialize($data));
        }
    }

    // Метод отправляет сообщение на message broker
    protected function publishFile($content){
        $msg = new AMQPMessage($content);

        $this->channel->basic_publish($msg, $this->amqp_config['exchange'], $this->amqp_config['routing_key']);

        $this->channel->close();
        $this->connection->close();
    }

    // Получение контента файла
    protected function getFileContent($file_path){
        if(file_exists($file_path)){
            $content = file_get_contents($file_path);
            unlink($file_path);

            return $content;
        }else{
            return false;
        }
    }


}
